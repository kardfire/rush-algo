/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alum.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 18:56:58 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/21 21:06:41 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALUM_H
# define ALUM_H

# include "libft.h"
# include "get_next_line.h"

# include <fcntl.h>

# define ENDTAB 424242
# define USER 1
# define IA 2

typedef struct		s_board
{
	char			*player_one;
	char			*player_two;
	size_t			*tab;
	size_t			game_mode;
	size_t			nb_row;
	size_t			nb_total_matches;
}					t_board;

typedef struct		s_row
{
	size_t			nb_matches;
	struct s_row	*next;
}					t_row;

void				name_select(t_board *board);
void				menu_select(t_board *board);
void				parse_std(t_board *board);
void				print_board(t_board board);
void				print_header(void);
void				parse_file(t_board *board, int fd);
void				game(t_board *board);
void				exit_error(void);
void				exit_free(t_row **list);
void				ft_freestruct(t_board *board);

void				ia_play(t_board *board, size_t cur_row, size_t matches,
							size_t cur_player);

int					ft_chifumi(t_board *board);

size_t				get_cur_row(t_board *board);

#endif
