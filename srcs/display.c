/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 19:01:24 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/21 21:26:14 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static void	print_occurence(char *occ, size_t nb, int nl)
{
	size_t i;

	i = 0;
	while (i <= nb)
	{
		ft_putstr(occ);
		i++;
	}
	if (nl == 1)
		ft_putchar('\n');
}

static void	print_row(size_t nb_matches)
{
	size_t	i;
	size_t	space;

	i = 0;
	if (nb_matches > 50)
		space = 0;
	else
		space = 50 - nb_matches;
	while (i++ < space)
		ft_putchar(' ');
	i = 0;
	while (i++ < 50 && i <= nb_matches)
		ft_putstr("| ");
	i = 0;
	while (i++ < space)
		ft_putchar(' ');
	ft_putstr("[x");
	ft_putnbr(nb_matches);
	ft_putstr("]\n");
}

void		print_board(t_board board)
{
	size_t i;

	i = 0;
	print_occurence("-", 107, 1);
	while (board.tab[i] != ENDTAB)
	{
		print_row(board.tab[i]);
		i++;
	}
	print_occurence("-", 107, 1);
}

void		print_header(void)
{
	print_occurence("#", 107, 0);
	ft_putstr("\n#");
	print_occurence(" ", 105, 0);
	ft_putstr("#\n#");
	print_occurence(" ", 37, 0);
	ft_putstr("Welcome to Alum-1 the new AAA game");
	print_occurence(" ", 33, 0);
	ft_putstr("#\n#");
	print_occurence(" ", 40, 0);
	ft_putstr("Prepare to fight NEXT GEN IA");
	print_occurence(" ", 36, 0);
	ft_putstr("#\n#");
	print_occurence(" ", 105, 0);
	ft_putstr("#\n#");
	print_occurence(" ", 39, 0);
	ft_putstr("Created by AKPENOU && BMARTINS");
	print_occurence(" ", 35, 0);
	ft_putstr("#\n#");
	print_occurence(" ", 105, 0);
	ft_putstr("#\n");
	print_occurence("#", 107, 1);
	ft_putchar('\n');
}
