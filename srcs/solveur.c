/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solveur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 15:37:23 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/21 21:27:04 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static size_t	ft_user_in(size_t nb_matches)
{
	int			rv;
	char		*s_in;
	size_t		input;

	rv = 0;
	input = 0;
	s_in = NULL;
	while ((rv = get_next_line(1, &s_in)) > 0 || 1)
	{
		if (ft_isdigit(*s_in) && ft_strlen(s_in) == 1)
			input = ft_atoi(s_in);
		if (input >= 1 && input <= 3 && input <= nb_matches)
			break ;
		ft_putendl("Please enter valid input");
	}
	if (rv < 0)
		exit_error();
	return (input);
}

void			human_play(t_board *board, size_t cur_row, size_t matches,
							size_t cur_player)
{
	size_t		input;

	if (cur_player == 1)
		ft_putstr(board->player_one);
	else
		ft_putstr(board->player_two);
	ft_putendl(" Your turn !");
	input = ft_user_in(matches);
	board->tab[cur_row] -= input;
	if (cur_player == 1)
		ft_putstr(board->player_one);
	else
		ft_putstr(board->player_two);
	ft_putstr(" Got ");
	ft_putnbr(input);
	ft_putendl(" matches");
}

static void		ft_play(t_board *board, size_t cur_player)
{
	size_t		matches;
	size_t		cur_row;

	cur_row = get_cur_row(board);
	matches = board->tab[cur_row];
	if (cur_player == 1)
	{
		if (board->game_mode == 1)
			human_play(board, cur_row, matches, cur_player);
		else if (board->game_mode == 2)
			human_play(board, cur_row, matches, cur_player);
		else
			ia_play(board, cur_row, matches, cur_player);
	}
	else
	{
		if (board->game_mode == 1)
			ia_play(board, cur_row, matches, cur_player);
		else if (board->game_mode == 2)
			human_play(board, cur_row, matches, cur_player);
		else
			ia_play(board, cur_row, matches, cur_player);
	}
}

static int		check_game_status(t_board *board, size_t cur_player)
{
	int			i;

	i = get_cur_row(board);
	if (i == 0)
	{
		if (board->tab[i] == 0)
		{
			if (cur_player == 1)
				ft_putstr(board->player_one);
			else
				ft_putstr(board->player_two);
			ft_putendl(" WIN !!!");
			return (1);
		}
	}
	return (0);
}

void			game(t_board *board)
{
	int			end;
	size_t		cur_player;

	end = 0;
	cur_player = ft_chifumi(board);
	while (end == 0)
	{
		print_board(*board);
		ft_play(board, cur_player);
		if (cur_player == 1)
			cur_player = 2;
		else
			cur_player = 1;
		end = check_game_status(board, cur_player);
	}
}
