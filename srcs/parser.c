/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 19:00:17 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/21 19:00:19 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static size_t	is_valid(char *line)
{
	int			i;

	i = 0;
	if (ft_strlen(line) > 5)
		return (0);
	while (line[i])
	{
		if (ft_isdigit(line[i]) == 0)
			return (0);
		i++;
	}
	i = ft_atoi(line);
	if (i < 1 || i > 10000)
		return (0);
	return ((size_t)i);
}

static size_t	add_new_row(t_row **list, char *line, t_board *board)
{
	int			matches;
	t_row		*tmp;
	t_row		*new_item;

	tmp = (*list);
	if ((matches = is_valid(line)) == 0)
		exit_free(list);
	new_item = (t_row *)malloc(sizeof(t_row));
	new_item->nb_matches = matches;
	new_item->next = NULL;
	if (*list == NULL)
	{
		board->nb_row = 1;
		(*list) = new_item;
	}
	else
	{
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = new_item;
		board->nb_row++;
	}
	return (matches);
}

static void		list_to_tab(t_row **list, t_board *board)
{
	int			i;
	t_row		*tmp;

	i = 0;
	board->tab = (size_t *)malloc(sizeof(size_t) * (board->nb_row + 2));
	while (*list)
	{
		tmp = (*list)->next;
		board->tab[i] = (*list)->nb_matches;
		i++;
		free(*list);
		(*list) = tmp;
	}
	board->tab[i] = ENDTAB;
}

void			parse_std(t_board *board)
{
	int			end;
	int			ret;
	char		*line;
	t_row		*list;

	end = 0;
	ret = 2;
	list = NULL;
	ft_putendl("For each row, enter number of matches btw 1 and 10000");
	while (!end && ((ret = get_next_line(0, &line)) > 0))
	{
		if (line[0])
			board->nb_total_matches += add_new_row(&list, line, board);
		else if (end == 0)
			end++;
		else
			exit_free(&list);
	}
	if (ret < 0)
		exit_free(&list);
	list_to_tab(&list, board);
}

void			parse_file(t_board *board, int fd)
{
	int			ret;
	char		*line;
	t_row		*list;

	ret = 2;
	list = NULL;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		if (line[0])
			board->nb_total_matches += add_new_row(&list, line, board);
		else
			exit_free(&list);
	}
	if (ret < 0)
		exit_free(&list);
	list_to_tab(&list, board);
}
