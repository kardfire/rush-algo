/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bmartins <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 11:47:12 by bmartins          #+#    #+#             */
/*   Updated: 2015/12/21 19:01:17 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

void	exit_free(t_row **list)
{
	t_row	*tmp;

	while (*list)
	{
		tmp = (*list)->next;
		free(*list);
		(*list) = tmp;
	}
	exit_error();
}

void	exit_error(void)
{
	ft_putendl("ERROR");
	exit(EXIT_FAILURE);
}
