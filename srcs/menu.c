/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   menu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bmartins <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 14:08:12 by bmartins          #+#    #+#             */
/*   Updated: 2015/12/21 20:49:17 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static void	print_menu(void)
{
	ft_putchar('\n');
	ft_putendl("Game mode:");
	ft_putendl("\t1 - Player VS IA");
	ft_putendl("\t2 - Player VS PLAYER");
	ft_putendl("\t3 - IA VS IA");
	ft_putchar('\n');
}

void		menu_select(t_board *board)
{
	int		rv;
	char	*line;

	rv = 0;
	line = NULL;
	print_menu();
	ft_putstr("Enter game mode: ");
	while (get_next_line(1, &line) > 0 || 1)
	{
		if (ft_isdigit(*line) && ft_strlen(line) == 1)
			board->game_mode = ft_atoi(line);
		if (board->game_mode >= 1 && board->game_mode <= 3)
			break ;
		ft_putstr("Please enter valid mode: ");
	}
	if (rv < 0)
		exit_error();
}

static char	*get_nick(void)
{
	int		rv;
	char	*line;

	rv = 0;
	line = NULL;
	ft_putstr("Enter a nickname: ");
	while (get_next_line(1, &line) != 1)
		;
	if (line[0] == '\0')
		return (ft_strdup("merde_ca_veux_pas_segfault"));
	else
		return (ft_strdup(line));
	if (rv < 0)
		exit_error();
}

void		name_select(t_board *board)
{
	if (board->game_mode == 1)
	{
		board->player_one = get_nick();
		board->player_two = ft_strdup("GET_REKT_NOOB");
	}
	else if (board->game_mode == 2)
	{
		board->player_one = get_nick();
		board->player_two = get_nick();
	}
	else
	{
		board->player_one = ft_strdup("Hans");
		board->player_two = ft_strdup("Gruber");
	}
	ft_putstr(board->player_one);
	ft_putstr(" VS ");
	ft_putendl(board->player_two);
}
