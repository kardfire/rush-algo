/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bmartins <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 12:03:43 by bmartins          #+#    #+#             */
/*   Updated: 2015/12/21 21:15:45 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

int			main(int argc, char **argv)
{
	int		fd;
	t_board	board;

	fd = 0;
	print_header();
	if (argc == 1)
		parse_std(&board);
	else if (argc == 2)
	{
		fd = open(argv[1], O_RDONLY);
		parse_file(&board, fd);
		close(fd);
	}
	else
		exit_error();
	ft_putendl("Current board");
	print_board(board);
	if (board.nb_row == 0)
		return (0);
	menu_select(&board);
	name_select(&board);
	game(&board);
	ft_freestruct(&board);
	return (0);
}

void		ft_freestruct(t_board *board)
{
	if (board->player_one)
		free(board->player_one);
	if (board->player_two)
		free(board->player_two);
	if (board->tab)
		free(board->tab);
}

int			ft_chifumi(t_board *board)
{
	int		user;
	char	*line;

	if (board->game_mode == 2 || board->game_mode == 3)
	{
		ft_putendl("Joueur 1 commence");
		return (1);
	}
	ft_putendl("1.. 2.. 3.. \n\t1 - Rock\n\t2 - Leaf\n\t3 - Scissors\n");
	while (get_next_line(1, &line) > 0 || 1)
	{
		if (ft_isdigit(*line) && ft_strlen(line) == 1)
			user = ft_atoi(line);
		if (user >= 1 && user <= 3)
			break ;
		ft_putstr("Please enter valid choice: ");
	}
	if (user == 1)
		ft_putendl("IA chose Leaf\n IA begin");
	if (user == 2)
		ft_putendl("IA chose Scissors\n IA begin");
	if (user == 3)
		ft_putendl("IA chose Rock\n IA begin");
	return (2);
}
