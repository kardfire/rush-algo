/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 21:00:41 by akpenou           #+#    #+#             */
/*   Updated: 2015/12/21 22:30:49 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "alum.h"

static char		ft_solve(size_t *tab)
{
	size_t		i;
	char		*choice;
	char		jeu;

	i = 0;
	while (tab[i] != ENDTAB && tab[i] != 0)
		++i;
	choice = (char *)malloc(sizeof(char) * (++i + 1));
	ft_putendl(choice);
	ft_putendl("TEST");
	choice[i] = '\0';
	i = 0;
	while (tab[i] != ENDTAB)
	{
		if (!(tab[i] % 5) && i > 0 && choice[i - 1] == 'P')
			choice[i] = 'G';
		else if (tab[i] % 5 && i > 0  && choice[i - 1] == 'P')
			choice[i] = 'P';
		else if (!(tab[i] % 5))
			choice[i] = 'P';
		else if (tab[i] % 5)
			choice[i] = 'G';
		++i;
		ft_putnbr(i);
	}
	ft_putendl(choice);
	jeu = choice[--i];
	free(choice);
	return (jeu);
}

void			ia_play(t_board *board, size_t cur_row, size_t matches,
	size_t cur_player)
{
	int			ia_input;

	if (ft_solve(board->tab) == 'P')
	{
		if (matches % 5 > 3 || matches % 5 == 0)
			ia_input = 1;
		else
			ia_input = matches % 5;
	}
	else
	{
		if (matches % 5 > 1)
			ia_input = matches % 5 - 1;
		else
			ia_input = 1;
	}
	if (cur_player == 1)
		ft_putstr(board->player_one);
	else
		ft_putstr(board->player_two);
	ft_putstr(" Got ");
	ft_putnbr(ia_input);
	ft_putendl(" matches");
	board->tab[cur_row] -= ia_input;
}

size_t			get_cur_row(t_board *board)
{
	size_t		i;

	i = board->nb_row - 1;
	while (board->tab[i] == 0 && i != 0)
		i--;
	return (i);
}
